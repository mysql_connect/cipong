#ifndef CONFIG_HPP
#define CONFIG_HPP


#define PROJECT_NAME "CiPong"

#define PROJECT_VERSION "1.0.0"

#define MINGW_VERSION "x86_64"

#define CMAKE_BUILD_TYPE "release"

#define VERSION_MAJOR 1
#define VERSION_MINOR 0
#define VERSION_PATCH 0


#endif //CONFIG_HPP
