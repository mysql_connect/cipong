#ifndef CIPONG_RESOURCE_HPP
#define CIPONG_RESOURCE_HPP

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

struct Resource {
    // Fonst
    sf::Font opensans;

    // Sounds
    sf::SoundBuffer ballsound;
    sf::SoundBuffer backsound;

    // Tesxtures
    sf::Texture logo;

    sf::Texture loading;
    sf::Texture backgrounds[7];

    sf::Texture top, bottom;

    sf::Texture balls[5];
    sf::Texture rockets[5];

};

extern Resource* g_resource;

#endif //CIPONG_RESOURCE_HPP