#include "Application.hpp"

#include "utils/Information.hpp"
#include "states/LoadingState.hpp"

Application::Application(Settings& settings, Data& data) : m_context(settings) {
	pushState<LoadingState>(*this);

	g_data = data;
	g_settings = settings;
}

void Application::runGameLoop() {
	sf::Clock dtTimer;
	sf::Clock dt;

	sf::Time m;

	while(m_context.window.isOpen() && !m_states.empty()) {
		auto deltaTime = dtTimer.restart();
		g_info.deltaTime = deltaTime.asSeconds();

		auto& state = *m_states.back();

		state.update(deltaTime.asSeconds());
		state.render(m_renderMaster);

		m_renderMaster.render(m_context.window);

		handleEvents();

		if (m_shouldPop) {
			m_shouldPop = false;
			m_states.pop_back();
		}

		m = dt.restart();
		g_info.elapsedTime += m.asSeconds();
	}
}


void Application::handleEvents() {

	sf::Event event;

	while (m_context.window.pollEvent(event)) {

		getCurrentState().handleEvent(event);

		switch (event.type) {
			case sf::Event::Closed:
				m_context.window.close();
				break;

			default:
				break;
		}

	}

}

// Выгрузка последней сцены
void Application::popState() {
	m_shouldPop = true;
}

// Получение текущей сцены
StateBase& Application::getCurrentState() {
	return *m_states.back();
}

