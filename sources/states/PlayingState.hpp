#ifndef PLAYINGSTATE_HPP
#define PLAYINGSTATE_HPP

#include <SFML/Audio.hpp>

#include "Resource.hpp"
#include "StateBase.hpp"

#include "interface/Menu.hpp"
#include "interface/Background.hpp"

struct Moved {
	bool up = false;
	bool down = false;
};

class PlayingState : public StateBase {

public:
	PlayingState(Application& application, const Resource& resource);
	~PlayingState();

	void update(float deltaTime) override;

	void handleEvent(sf::Event event) override;

	void render(RenderMaster& renderer) override;

private:
	Interface::Background m_background;

	Interface::Menu pause, gameOver;

	sf::Text m_text;

    bool isPlaying = false, isGameOver = false;
    bool isPause = false, fixESC = false;

	sf::Sprite m_spriteTop;
	sf::Sprite m_spriteBottom;

    float scale;

	int rocketSpeed;
	sf::Vector2f rocketSize;
	sf::RectangleShape m_leftRocket, m_rightRocket;

	Moved left, right;

	sf::Sound m_ballsound;

	sf::CircleShape m_ball;
    int ballRadius, ballAngle, ballSpeed;

    sf::Clock AITimer; const sf::Time AITime = sf::seconds(0.1f);

    sf::Text m_scoreText;
    unsigned int m_scoreCount = 1;
    unsigned int m_score = 0;

    void scoreUpdate(int plus);
};


#endif //PLAYINGSTATE_HPP
