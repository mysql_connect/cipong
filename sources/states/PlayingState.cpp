#include "PlayingState.hpp"

#include <math.h>
#include <fstream>

#include "Data.hpp"
#include "Context.hpp"
#include "Application.hpp"

#include "utils/Random.hpp"
#include "utils/ResourceHolder.hpp"

#include "interface/InputBox.hpp"
#include "interface/BasicButton.hpp"
#include "interface/ToggleButton.hpp"

PlayingState::PlayingState(Application& application, const Resource& resource)
: StateBase(application)
, m_background(resource)
, pause(L"Пауза")
, gameOver(L"Игра окончена") {
    scale = (*g_window).getSize().x / 1280.f * 1.5f;

    m_spriteTop.setTexture(resource.top);
    m_spriteTop.setOrigin(m_spriteTop.getTexture()->getSize().x / 2, 0);
    m_spriteTop.setPosition((*g_window).getSize().x / 2, 0);

    m_spriteBottom.setTexture(resource.bottom);
    m_spriteBottom.setTextureRect({0, 0, static_cast<int>((*g_window).getSize().x), static_cast<int>(m_spriteBottom.getTexture()->getSize().y)});
    m_spriteBottom.setPosition(0, (*g_window).getSize().y - m_spriteBottom.getTexture()->getSize().y);

    m_text.setFont(g_resource->opensans);
    m_text.setCharacterSize(24);
    m_text.setString(L"Нажмите <Space/Пробел> что бы начать");
    m_text.setPosition((*g_window).getSize().x / 2 - m_text.getGlobalBounds().width / 2, (*g_window).getSize().y - 100);

    rocketSize = {40, 110};

    m_leftRocket.setSize(rocketSize);
    m_leftRocket.setScale(scale, scale);
    m_leftRocket.setTexture(&g_resource->rockets[g_data.rocket]);
    m_leftRocket.setOrigin(m_leftRocket.getSize() / 2.f);
    m_leftRocket.setPosition(rocketSize.x / 2 * scale, (*g_window).getSize().y / 2 + 30);

    m_rightRocket.setSize(rocketSize);
    m_rightRocket.setScale(-scale, scale);
    m_rightRocket.setOrigin(m_leftRocket.getSize() / 2.f);
    m_rightRocket.setTexture(&g_resource->rockets[RandomSingleton::get().intInRage(0, 4)]);
    m_rightRocket.setPosition((*g_window).getSize().x - rocketSize.x / 2 * scale, (*g_window).getSize().y / 2 + 30);

    rocketSpeed = 256 * (*g_window).getSize().y / 720;

    m_ballsound.setVolume(4);
    m_ballsound.setBuffer(g_resource->ballsound);

    ballRadius = 12;

    m_ball.scale(scale, scale);
    m_ball.setRadius(ballRadius);
    m_ball.setOrigin(ballRadius, ballRadius);
    m_ball.setTexture(&g_resource->balls[g_data.ball]);
    m_ball.setPosition((*g_window).getSize().x / 2, (*g_window).getSize().y / 2 + 30);

    pause.addComponent(std::make_unique<Interface::BasicButton>(L" Продолжить ", [&] () {
        isPause = false;
    }));

    pause.addComponent(std::make_unique<Interface::BasicButton>(L"Главное меню", [&] () {
        m_application->popState();
    }));

    gameOver.addComponent(std::make_unique<Interface::BasicButton>(L" Перезапуск  ", [&] () {
        ballSpeed = 384;
        m_scoreCount = 1;m_score = 0;
        m_ball.setPosition((*g_window).getSize().x / 2, (*g_window).getSize().y / 2 + 30);

        do {
            ballAngle = RandomSingleton::get().intInRage(-180, 180);
        } while (std::abs(ballAngle) > 45 && std::abs(ballAngle) < 135);

        m_leftRocket.setPosition(rocketSize.x / 2 * scale, (*g_window).getSize().y / 2 + 30);

        m_rightRocket.setTexture(&g_resource->rockets[RandomSingleton::get().intInRage(0, 4)]);
        m_rightRocket.setPosition((*g_window).getSize().x -  rocketSize.x / 2 * scale, (*g_window).getSize().y / 2 + 30);

        isPlaying = false; isGameOver = false; isPause = false;
    }));

    gameOver.addComponent(std::make_unique<Interface::BasicButton>(L"Главное меню", [&] () {
        m_application->popState();
    }));

    ballSpeed = 384;

    do {
        ballAngle = RandomSingleton::get().intInRage(-180, 180);
    } while (std::abs(ballAngle) >= 45 && std::abs(ballAngle) <= 135);

    m_scoreText.setCharacterSize(24);
    m_scoreText.setFont(g_resource->opensans);
    scoreUpdate(0);
}

PlayingState::~PlayingState() {
    pause.clear();
}

void PlayingState::update(float deltaTime) {
    m_background.update(deltaTime);

    /// Пока пауза останавливаем
    if (isPause) {
        pause.update(deltaTime);
        return;
    }

    /// Пока гейм овер останавливаем
    if (isGameOver) {
        gameOver.update(deltaTime);
        return;
    }

    /// Пока игра не начата останавливаем
    if (!isPlaying) {
        return;
    }

    /// Кручение мяча
    if (ballAngle > 0) {m_ball.rotate(ballSpeed * deltaTime);} else {m_ball.rotate(-ballSpeed * deltaTime);}

    /// Перемещение мяча
    m_ball.move(std::cos(ballAngle * M_PI / 180) * deltaTime * ballSpeed, std::sin(ballAngle * M_PI / 180) * deltaTime * ballSpeed);

    /// АИ логика бота
    if (AITimer.getElapsedTime() > AITime) {
        AITimer.restart();
        if (m_ball.getPosition().y + ballRadius * scale > m_rightRocket.getPosition().y + rocketSize.y / 2 * scale) {
            right.up = false;
            right.down = true;
        } else if (m_ball.getPosition().y - ballRadius * scale < m_rightRocket.getPosition().y - rocketSize.y / 2 * scale) {
            right.up = true;
            right.down = false;
        } else {
            right.up = false;
            right.down = false;
        }
    }

    /// Движение левой рокетки вверх
    if (left.up && m_leftRocket.getPosition().y - rocketSize.y / 2 * scale > 62) {
        m_leftRocket.move(0, -deltaTime * rocketSpeed);
    }

    /// Движение левой рокетки вниз
    if (left.down && m_leftRocket.getPosition().y + rocketSize.y / 2 * scale < (*g_window).getSize().y - 37) {
        m_leftRocket.move(0, deltaTime * rocketSpeed);
    }

    /// Движение правой рокетки вверх
    if (right.up && m_rightRocket.getPosition().y - rocketSize.y / 2 * scale > 62) {
        m_rightRocket.move(0, -deltaTime * rocketSpeed);
    }

    /// Движение правой рокетки вниз
    if (right.down && m_rightRocket.getPosition().y + rocketSize.y / 2 * scale < (*g_window).getSize().y - 37) {
        m_rightRocket.move(0, deltaTime * rocketSpeed);
    }

    /// Колизия с левой гранью
    if (m_ball.getPosition().x - ballRadius * scale <= 0) {
        isGameOver = true;
        g_data.money += m_score;
		std::ofstream outDataFile("data.bin", std::ios::binary);
		outDataFile.write((char*)&g_data.money, sizeof(g_data.money));
		outDataFile.write((char*)g_data.balls, sizeof(g_data.balls));
		outDataFile.write((char*)g_data.rockets, sizeof(g_data.rockets));
		outDataFile.write((char*)&g_data.ball, sizeof(g_data.ball));
		outDataFile.write((char*)&g_data.rocket, sizeof(g_data.rocket));
		outDataFile.close();
    }

    /// Колизия с правой гранью
    if (m_ball.getPosition().x + ballRadius * scale >= (*g_window).getSize().x) {
        m_ball.setPosition((*g_window).getSize().x - ballRadius * scale - 0.1f, m_ball.getPosition().y);
        ballAngle = -ballAngle;ballSpeed = -ballSpeed;
        scoreUpdate(m_scoreCount);
        ballSpeed = ballSpeed * 0.95;
        m_scoreCount++;
    }

    /// Колизия левой рокетки
    if (m_ball.getPosition().x - ballRadius * scale <  m_leftRocket.getPosition().x + rocketSize.x / 2 * scale - 10 * scale &&
        m_ball.getPosition().y + ballRadius * scale >= m_leftRocket.getPosition().y - rocketSize.y / 2 * scale &&
        m_ball.getPosition().y - ballRadius * scale <= m_leftRocket.getPosition().y + rocketSize.y / 2 * scale) {

        if (m_ball.getPosition().y > m_leftRocket.getPosition().y) {
            ballAngle = -ballAngle + RandomSingleton::get().intInRage(0, 10);ballSpeed = -ballSpeed;
        } else {
            ballAngle = -ballAngle - RandomSingleton::get().intInRage(0, 10);ballSpeed = -ballSpeed;
        }
        m_ball.setPosition(m_leftRocket.getPosition().x + ballRadius * scale + rocketSize.x / 2 * scale - 10 * scale + 0.1f, m_ball.getPosition().y);
        if (ballSpeed > 0) {ballSpeed += 32;} else {ballSpeed -= 32;}
        if (g_settings.isSoundPlay) m_ballsound.play();
    }

    /// Колизия правой рокетки
    if (m_ball.getPosition().x + ballRadius * scale >  m_rightRocket.getPosition().x - rocketSize.x / 2 * scale + 10 * scale &&
        m_ball.getPosition().y + ballRadius * scale >= m_rightRocket.getPosition().y - rocketSize.y / 2 * scale &&
        m_ball.getPosition().y - ballRadius * scale <= m_rightRocket.getPosition().y + rocketSize.y / 2 * scale) {

        if (m_ball.getPosition().y > m_rightRocket.getPosition().y) {
            ballAngle = -ballAngle + RandomSingleton::get().intInRage(0, 10);ballSpeed = -ballSpeed;
        } else {
            ballAngle = -ballAngle - RandomSingleton::get().intInRage(0, 10);ballSpeed = -ballSpeed;
        }
        m_ball.setPosition(m_rightRocket.getPosition().x - ballRadius * scale - rocketSize.x / 2 * scale + 10 * scale - 0.1f, m_ball.getPosition().y);
        if (ballSpeed > 0) {ballSpeed += 32;} else {ballSpeed -= 32;}
        if (g_settings.isSoundPlay) m_ballsound.play();
    }

    /// Колизия с верхней полосой
    if (m_ball.getPosition().y - ballRadius * scale - 57 <= 0) {
        m_ball.setPosition(m_ball.getPosition().x, ballRadius * scale + 57 + 0.1f);
        ballAngle = -ballAngle;
        if (g_settings.isSoundPlay) m_ballsound.play();
    }

    /// Колизия с нижней полосой
    if (m_ball.getPosition().y + ballRadius * scale + 27 >= (g_window->getSize().y)) {
        m_ball.setPosition(m_ball.getPosition().x, (*g_window).getSize().y - ballRadius * scale- 27 + 0.1f);
        ballAngle = -ballAngle;
        if (g_settings.isSoundPlay) m_ballsound.play();
    }

}

void PlayingState::handleEvent(sf::Event event) {
    /// Отлавливание кнопки ESC
    if (!isGameOver) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            if (!fixESC) {
                fixESC = true;
                if (!isPause) {
                    isPause = true;
                } else {
                    isPause = false;
                }
            }
        } else {
            fixESC = false;
        }
    }

    if (!isPause && !isGameOver && !isPlaying) {
        /// Отлавливание кнопки Space
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            isPlaying = true;
        }
    }

    if (!isPause && !isGameOver && isPlaying) {
        /// Отлавливание кнопки W или Up
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {left.up = true;} else {left.up = false;}

        /// Отлавливание кнопки S или Down
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {left.down = true;} else {left.down = false;}
    }

    if (isPause) {
        pause.handleEvent(event);
    }

    if (isGameOver) {
        gameOver.handleEvent(event);
    }
}

void PlayingState::render(RenderMaster& renderer) {
    m_background.render(renderer);
    renderer.drawSFML(m_spriteTop);
    renderer.drawSFML(m_spriteBottom);

    if (!isPause && isPlaying) {
        renderer.drawSFML(m_leftRocket);
        renderer.drawSFML(m_rightRocket);

        renderer.drawSFML(m_ball);
    }

    if (!isPlaying && !isPause) {
        renderer.drawSFML(m_text);
    }

    if (isPause) {
        pause.render(renderer);
    }

    if (isGameOver) {
        gameOver.render(renderer);
    }

    renderer.drawSFML(m_scoreText);
}

void PlayingState::scoreUpdate(int plus) {
    m_score += plus;
    m_scoreText.setString(std::to_wstring(m_score));
    m_scoreText.setPosition((*g_window).getSize().x / 2 - m_scoreText.getGlobalBounds().width / 2, 12);
}
