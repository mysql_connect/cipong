#include "MenuState.hpp"

#include <cstdlib>
#include <fstream>

#include "../Config.hpp"
#include "Settings.hpp"
#include "Application.hpp"

#include "interface/Text.hpp"
#include "interface/ShopCell.hpp"
#include "interface/InputBox.hpp"
#include "interface/ShopButton.hpp"
#include "interface/ImageButton.hpp"
#include "interface/BasicButton.hpp"
#include "interface/ToggleButton.hpp"

#include "PlayingState.hpp"

MenuState::MenuState(Application& application, const Resource& resource)
: StateBase(application)
, m_music(resource)
, m_background(resource)
, m_setting(L"Настройки игры", {750, 370})
, m_shop(L"Магазин игровых скинов", {750, 490}) {
    tempData = g_data;
	initHeader(resource);
	initBigboard(resource);
}

MenuState::~MenuState() {
	m_bigboard.clear();
	m_setting.clear();
	m_shop.clear();
}

void MenuState::initHeader(const Resource& resource) {
	m_header.addButton(std::make_unique<Interface::ImageButton>(g_resource->logo , L"CiPong One", [&]() {
		activeMenu = 0;
	}), 25, 0);
	m_header.addButton(std::make_unique<Interface::BasicButton>(L"ИГРАТЬ", [&]() {
		m_application->pushState<PlayingState>(*m_application, resource);
		activeMenu = 0;
	}, 1), 10, 0);
	m_header.addButton(std::make_unique<Interface::BasicButton>(L"Магазин игровых скинов", [&]() {
	    m_shop.clear();
	    initShop(resource);
        activeMenu = 2;
	}, 0), 0, 0);
	m_header.addButton(std::make_unique<Interface::BasicButton>(L"Игровые настройки", [&]() {
	    m_setting.clear();
	    initSetting(resource);
		activeMenu = 4;
	}, 0), 0, 0);

    m_header.addButton(std::make_unique<Interface::BasicButton>(L"Выход", [&]() {
        (*g_window).close();
    }, 0), 0, 1);
}

void MenuState::initShop(const Resource& resource) {
    m_shop.clear();
    m_shop.addComponent(std::make_unique<Interface::Text>(14, L"Ваш баланс: " + std::to_wstring(g_data.money) + L"$"), {20, 50});

    if (shopType == 0) {
        for (int i = 0; i < 5; i++) {
            Interface::ShopCell rocket(L"Ракетка #" + std::to_wstring(i + 1), i * 50, {225, 150});
            rocket.addImage(resource.rockets[i], {40, 110});
            if (g_data.rockets[i] == false) {
                if (i * 50 > g_data.money) {
                    rocket.setStatis(L"Не хватает $");
                } else {
                    rocket.setStatis(L"Купи меня :3");
                }
            } else {
                if (g_data.rocket == i) {
                    rocket.setStatis(L"Я весь твой :)");
                } else {
                    rocket.setStatis(L"Выбери меня :)");
                }
            }
            sf::Vector2f shopcell, basicbutton;
            if (i < 3) {
                shopcell = {18.75f * (i + 1) + 225 * i, 75};
                basicbutton = {18.75f * (i + 1) + 225 * i + 10, 75 + 100};
            } else {
                shopcell = {131.25f + (225 + 18.75f) * (i - 3), 243.75f};
                basicbutton = {131.25f + (225 + 18.75f) * (i - 3) + 10, 243.75f + 100};
            }
            m_shop.addComponent(std::make_unique<Interface::ShopCell>(rocket), shopcell);
            if (g_data.rockets[i] == false) {
                m_shop.addComponent(std::make_unique<Interface::ShopButton>(L"Купить", [&](int id, Data& f_data) {
                    if (id * 50 <= f_data.money) {
                        f_data.rockets[id] = true;
                        f_data.money -= id * 50;
                        f_data.rocket = id;
						std::ofstream outDataFile("data.bin", std::ios::binary);
						outDataFile.write((char*)&f_data.money, sizeof(f_data.money));
						outDataFile.write((char*)f_data.balls, sizeof(f_data.balls));
						outDataFile.write((char*)f_data.rockets, sizeof(f_data.rockets));
						outDataFile.write((char*)&f_data.ball, sizeof(f_data.ball));
						outDataFile.write((char*)&f_data.rocket, sizeof(f_data.rocket));
						outDataFile.close();
                    }
                }, i, g_data), basicbutton);
            } else {
                m_shop.addComponent(std::make_unique<Interface::ShopButton>(L"Выбрать", [&](int id, Data& f_data) {
                    f_data.rocket = id;
					std::ofstream outDataFile("data.bin", std::ios::binary);
					outDataFile.write((char*)&f_data.money, sizeof(f_data.money));
					outDataFile.write((char*)f_data.balls, sizeof(f_data.balls));
					outDataFile.write((char*)f_data.rockets, sizeof(f_data.rockets));
					outDataFile.write((char*)&f_data.ball, sizeof(f_data.ball));
					outDataFile.write((char*)&f_data.rocket, sizeof(f_data.rocket));
					outDataFile.close();
                }, i, g_data), basicbutton);
            }
        }
    } else {
        for (int i = 0; i < 5; i++) {
            Interface::ShopCell ball(L"Шар #" + std::to_wstring(i + 1), i * 50, {225, 150});
            ball.addImage(resource.balls[i], {40, 40});
            if (g_data.balls[i] == false) {
                if (i * 50 > g_data.money) {
                    ball.setStatis(L"Не хватает $");
                } else {
                    ball.setStatis(L"Купи меня :3");
                }
            } else {
                if (g_data.ball == i) {
                    ball.setStatis(L"Я весь твой :)");
                } else {
                    ball.setStatis(L"Выбери меня :)");
                }
            }
            sf::Vector2f shopcell, basicbutton;
            if (i < 3) {
                shopcell = {18.75f * (i + 1) + 225 * i, 75};
                basicbutton = {18.75f * (i + 1) + 225 * i + 10, 75 + 100};
            } else {
                shopcell = {131.25f + (225 + 18.75f) * (i - 3), 243.75f};
                basicbutton = {131.25f + (225 + 18.75f) * (i - 3) + 10, 243.75f + 100};
            }
            m_shop.addComponent(std::make_unique<Interface::ShopCell>(ball), shopcell);
            if (g_data.balls[i] == false) {
                m_shop.addComponent(std::make_unique<Interface::ShopButton>(L"Купить", [&](int id, Data& f_data) {
                    if (id * 50 <= f_data.money) {
                        f_data.balls[id] = true;
                        f_data.money -= id * 50;
                        f_data.ball = id;
                        std::ofstream outDataFile("data.bin", std::ios::binary);
                        outDataFile.write((char*)&f_data.money, sizeof(f_data.money));
                        outDataFile.write((char*)f_data.balls, sizeof(f_data.balls));
                        outDataFile.write((char*)f_data.rockets, sizeof(f_data.rockets));
                        outDataFile.write((char*)&f_data.ball, sizeof(f_data.ball));
                        outDataFile.write((char*)&f_data.rocket, sizeof(f_data.rocket));
                        outDataFile.close();
                    }
                }, i, g_data), basicbutton);
            } else {
                m_shop.addComponent(std::make_unique<Interface::ShopButton>(L"Выбрать", [&](int id, Data& f_data) {
                    f_data.ball = id;
                    std::ofstream outDataFile("data.bin", std::ios::binary);
                    outDataFile.write((char*)&f_data.money, sizeof(f_data.money));
                    outDataFile.write((char*)f_data.balls, sizeof(f_data.balls));
                    outDataFile.write((char*)f_data.rockets, sizeof(f_data.rockets));
                    outDataFile.write((char*)&f_data.ball, sizeof(f_data.ball));
                    outDataFile.write((char*)&f_data.rocket, sizeof(f_data.rocket));
                    outDataFile.close();
                }, i, g_data), basicbutton);
            }
        }
    }

    m_shop.addComponent(std::make_unique<Interface::BasicButton>(L"Вернуться в главное меню", [&]() {
        activeMenu = 0;
    }, 3), {82, 420});

    m_shop.addComponent(std::make_unique<Interface::BasicButton>(L"Следующий магазин ==>", [&]() {
        if (shopType == 0) shopType = 1; else shopType = 0;
        initShop(resource);
    }, 3), {383, 420});

}

void MenuState::initSetting(const Resource &resource) {
    m_setting.addComponent(
            std::make_unique<Interface::InputBox>(4, sf::Vector2f(230, 40), L"Ширина экрана:", g_settings.windowX),
            {10, 80}
    );

    m_setting.addComponent(
            std::make_unique<Interface::InputBox>(4, sf::Vector2f(230, 40), L"Высота экрана:", g_settings.windowY),
            {10, 170}
    );

	m_setting.addComponent(std::make_unique<Interface::ToggleButton<bool>>(
			L"На весь экран", std::vector<std::pair<std::wstring, bool>>{{L"Выкл", false}, {L"Вкл", true}}, g_settings.isFullscreen
	), {260, 90});

    m_setting.addComponent(std::make_unique<Interface::ToggleButton<bool>>(
            L"Звуки в игре", std::vector<std::pair<std::wstring, bool>>{{L"Выкл", false}, {L"Вкл", true}}, g_settings.isSoundPlay
    ), {260, 180});

    m_setting.addComponent(std::make_unique<Interface::ToggleButton<bool>>(
            L"V-Syns", std::vector<std::pair<std::wstring, bool>>{{L"Выкл", false}, {L"Вкл", true}}, g_settings.verticalSync
    ), {510, 90});

    m_setting.addComponent(std::make_unique<Interface::ToggleButton<int>>(
            L"Сглаживание", std::vector<std::pair<std::wstring, int>>{{L"Выкл", 0}, {L"x2", 2}, {L"x4", 4}, {L"x8", 8}}, g_settings.antialiasing
    ), {510, 180});

    m_setting.addComponent(std::make_unique<Interface::BasicButton>(L"Вернуться в главное меню", [&]() {
        activeMenu = 0;
    }, 3), {82, 300});

    m_setting.addComponent(std::make_unique<Interface::BasicButton>(L"Применить настройки", [&]() {
        std::ofstream configFile("settings.txt");

        if (configFile.is_open()) {
            configFile << "fullscreen " + std::to_string(g_settings.isFullscreen) << std::endl;
            configFile << "issoundplay " + std::to_string(g_settings.isSoundPlay) << std::endl;
            configFile << "verticalsync " + std::to_string(g_settings.verticalSync) << std::endl;
            configFile << "antiliasing " + std::to_string(g_settings.antialiasing) << std::endl;
            configFile << "windowsize " + std::to_string(g_settings.windowX) << " " << std::to_string(g_settings.windowY) << std::endl;
        }

        configFile.close();

        std::string startCommand = "start " + static_cast<std::string>(PROJECT_NAME) + "_" + static_cast<std::string>(PROJECT_VERSION) + "_" + static_cast<std::string>(MINGW_VERSION) + "_" + static_cast<std::string>(CMAKE_BUILD_TYPE);

        system(startCommand.c_str());
        (*g_window).close();
    }, 3), {383, 300});

    m_setting.addComponent(std::make_unique<Interface::Text>(16, L"ПРИМЕЧАНИЕ: Применение настроек приведет перезапуску приложения!") , {82, 265});
}

void MenuState::initBigboard(const Resource& resource) {
	m_bigboard.addComponent(std::make_unique<Interface::Text>(78, L"Играйте в пинг понг "), 20);
	m_bigboard.addComponent(std::make_unique<Interface::Text>(24, L"Лучшая игра для времяпровождения в скучные моменты жизни "), 50);
	m_bigboard.addComponent(std::make_unique<Interface::BasicButton>(L"НАЧАТЬ ИГРУ", [&]() {
		m_application->pushState<PlayingState>(*m_application, resource);
	}, 2));
}

void MenuState::update(float deltaTime) {
    if (tempData.money != g_data.money || tempData.rocket != g_data.rocket || tempData.ball != g_data.ball) {
        tempData = g_data;
        initShop(*g_resource);
    }

	m_background.update(deltaTime);
	m_header.update(deltaTime);

	if (activeMenu == 0) m_bigboard.update(deltaTime);
	if (activeMenu == 2) m_shop.update(deltaTime);
	if (activeMenu == 4) m_setting.update(deltaTime);

    m_music.update();
}

void MenuState::handleEvent(sf::Event event) {
	m_header.handleEvent(event);

	if (activeMenu == 0) m_bigboard.handleEvent(event);
    if (activeMenu == 2) m_shop.handleEvent(event);
	if (activeMenu == 4) m_setting.handleEvent(event);
}

void MenuState::render(RenderMaster& renderer) {
	m_background.render(renderer);
	m_header.render(renderer);

	if (activeMenu == 0) m_bigboard.render(renderer);
    if (activeMenu == 2) m_shop.render(renderer);
	if (activeMenu == 4) m_setting.render(renderer);
}