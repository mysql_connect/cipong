#ifndef STATEBASE_HPP
#define STATEBASE_HPP

#include <SFML/Graphics.hpp>

class RenderMaster;
class Application;

class StateBase {

public:
	StateBase(Application& application);

	virtual ~StateBase() = default;

	virtual void update(float deltaTime) = 0;

	virtual void handleEvent(sf::Event event) = 0;

	virtual void render(RenderMaster& renderer) = 0;

protected:
	Application* m_application = nullptr;

};


#endif //STATEBASE_HPP
