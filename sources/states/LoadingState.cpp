#include <iostream>
#include "LoadingState.hpp"

#include "Context.hpp"
#include "MenuState.hpp"
#include "Application.hpp"
#include "utils/ResourceHolder.hpp"

LoadingState::LoadingState(Application& application) : StateBase(application) {
    sf::Texture texture;
    texture.loadFromFile("resource/textures/loading.png");
    float scale = static_cast<float>((*g_window).getSize().y) / texture.getSize().y;

    loading.setTexture(texture);
    loading.setScale(scale, scale);
    loading.setPosition((*g_window).getSize().x / 2, (*g_window).getSize().y / 2);
    loading.setOrigin(loading.getTexture()->getSize().x / 2, loading.getTexture()->getSize().y / 2);

    sf::Font font;
    font.loadFromFile("resource/fonts/OpenSans.ttf");

    statusText.setFont(font);
    statusText.setCharacterSize(18);
    statusText.setPosition(12, (*g_window).getSize().y - 38);

    statusPercent.setFont(font);
    statusPercent.setCharacterSize(18);
    statusPercent.setString(L"0%");
    statusPercent.setPosition((*g_window).getSize().x - statusPercent.getGlobalBounds().width - 18, (*g_window).getSize().y - 38);

    progressBar.setSize({static_cast<float>((*g_window).getSize().x - 24), 28});
    progressBar.setOutlineThickness(2);
    progressBar.setFillColor(sf::Color(60, 132, 139, 255));
    progressBar.setOutlineColor(sf::Color(67, 158, 178, 255));
    progressBar.setPosition(12, (*g_window).getSize().y - 40);

    loadingBar.setSize({static_cast<float>((*g_window).getSize().x - 24), 28});
    loadingBar.setFillColor(sf::Color(82, 162, 182, 255));
    loadingBar.setPosition(12, (*g_window).getSize().y - 40);

    count = 0;
    sizeStruct = sizeof(m_resource);

    updateSatus(L"Иницилизация ресурсов", 0);

    loadFonts();
    loadSounds();
    loadTextures();

    g_resource = &m_resource;

    updateSatus(L"Завершенно", 0);
}

LoadingState::~LoadingState() {}

void LoadingState::update(float deltaTime) {
    m_application->pushState<MenuState>(*m_application, m_resource);
}

void LoadingState::handleEvent(sf::Event event) {}

void LoadingState::render(RenderMaster &renderer) {}

void LoadingState::updateSatus(std::wstring status, int plus) {
    statusText.setString(status);
    statusPercent.setString(std::to_wstring((int)((float) count / sizeStruct * 100)) + L"%");
    statusPercent.setPosition((*g_window).getSize().x - statusPercent.getGlobalBounds().width - 18, (*g_window).getSize().y - 38);

    loadingBar.setScale((float) count / sizeStruct, 1);

    (*g_window).clear();
    (*g_window).draw(loading);
    (*g_window).draw(progressBar);
    (*g_window).draw(loadingBar);
    (*g_window).draw(statusText);
    (*g_window).draw(statusPercent);
    (*g_window).display();
    count += plus;
}

void LoadingState::loadFonts() {
    updateSatus(L"Загрузка шрифта - OpenSans", sizeof(m_resource.opensans));
    m_resource.opensans = ResourceHolder::getFont("OpenSans");
}

void LoadingState::loadSounds() {
    updateSatus(L"Загрузка ауди файла - backsound", sizeof(m_resource.backsound));
    m_resource.backsound = ResourceHolder::getSoundBuffer("backsound");

    updateSatus(L"Загрузка ауди файла - ball", sizeof(m_resource.ballsound));
    m_resource.ballsound = ResourceHolder::getSoundBuffer("ball");
}

void LoadingState::loadTextures() {
    updateSatus(L"Загрузка текстуры - logo", sizeof(m_resource.logo));
    m_resource.logo = ResourceHolder::getTexture("logo");

    updateSatus(L"Загрузка текстуры - loading", sizeof(m_resource.loading));
    m_resource.loading = ResourceHolder::getTexture("loading");

    for (int i = 0; i < 7; ++i) {
        updateSatus(L"Загрузка текстуры - backgrounds/" + std::to_wstring(i+1), sizeof(m_resource.backgrounds[i]));
        m_resource.backgrounds[i] = ResourceHolder::getTexture("background/" +  std::to_string(i+1));
    }

    updateSatus(L"Загрузка текстуры - top", sizeof(m_resource.top));
    m_resource.top = ResourceHolder::getTexture("top");

    updateSatus(L"Загрузка текстуры - bottom", sizeof(m_resource.bottom));
    m_resource.bottom = ResourceHolder::getTexture("bottom");
    m_resource.bottom.setRepeated(true);

    for (int i = 0; i < 5; ++i) {
        updateSatus(L"Загрузка текстуры - balls/" + std::to_wstring(i+1), sizeof(m_resource.balls[i]));
        m_resource.balls[i] = ResourceHolder::getTexture("balls/" +  std::to_string(i+1));
        m_resource.balls[i].setSmooth(true);
    }

    for (int i = 0; i < 5; ++i) {
        updateSatus(L"Загрузка текстуры - rockets/" + std::to_wstring(i+1), sizeof(m_resource.rockets[i]));
        m_resource.rockets[i] = ResourceHolder::getTexture("rockets/" +  std::to_string(i+1));
        m_resource.rockets[i].setSmooth(true);
    }
}
