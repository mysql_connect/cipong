#ifndef LOADINGSTATE_H
#define LOADINGSTATE_H

#include "Settings.hpp"
#include "StateBase.hpp"

#include "Resource.hpp"

class LoadingState : public StateBase {

public:
    LoadingState(Application& application);
    ~LoadingState();

    void update(float deltaTime);

    void handleEvent(sf::Event event);

    void render(RenderMaster& renderer);

private:
    Resource m_resource;

    void updateSatus(std::wstring status, int plus);

    void loadFonts();
    void loadSounds();
    void loadTextures();

    sf::Sprite loading;

    sf::RectangleShape progressBar;
    sf::RectangleShape loadingBar;

    sf::Text statusText;
    sf::Text statusPercent;

    int count;
    int sizeStruct;

};


#endif //LOADINGSTATE_H
