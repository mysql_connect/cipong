#ifndef MENUSTATE_HPP
#define MENUSTATE_HPP

#include "StateBase.hpp"
#include "Resource.hpp"
#include "Music.hpp"
#include "Data.hpp"

#include "interface/Background.hpp"
#include "interface/Bigboard.hpp"
#include "interface/Setting.hpp"
#include "interface/Header.hpp"
#include "interface/Menu.hpp"

class Settings;

class MenuState : public StateBase {

public:
	MenuState(Application& application, const Resource& resource);
	~MenuState();

	void update(float deltaTime) override;

	void handleEvent(sf::Event event) override;

	void render(RenderMaster& renderer) override;

private:
	Music m_music;
    Data tempData;

	Interface::Background m_background;
	Interface::Bigboard m_bigboard;
	Interface::Setting m_setting;
	Interface::Header m_header;
	Interface::Setting m_shop;

	int activeMenu = 0;
	int shopType = 0;

	void initShop(const Resource& resource);
	void initHeader(const Resource& resource);
	void initSetting(const Resource& resource);
    void initBigboard(const Resource& resource);

};


#endif //MENUSTATE_HPP
