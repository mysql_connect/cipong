#ifndef CIPONG_SHOPCELL_HPP
#define CIPONG_SHOPCELL_HPP

#include "Component.hpp"

namespace Interface {

class ShopCell : public Component {

public:
    ShopCell(const std::wstring& title, int price, const sf::Vector2f size);

    void addImage(const sf::Texture& texture, const sf::Vector2f size);
    void setStatis(const std::wstring& status);

    void update(float deltaTime) override;
    void handleEvent(const sf::Event& event) override;
    void render(RenderMaster& renderer) noexcept override;

    void setPosition(const sf::Vector2f& position) override;

    const sf::Vector2f getSize() const override;

private:
    sf::Text m_title;
    sf::Text m_price;
    sf::Text m_status;
    sf::RectangleShape m_quad;
    sf::RectangleShape m_image;

};

};


#endif //CIPONG_SHOPCELL_HPP
