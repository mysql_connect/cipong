#ifndef INTERFACE_BUTTON_HPP
#define INTERFACE_BUTTON_HPP

#include "Component.hpp"

namespace Interface {

class Button : public Component {

public:
	Button(const sf::RectangleShape& rectangle);

private:
	void update(float deltaTime) override;
	void handleEvent(const sf::Event& event) override;

	virtual void onClick() = 0;
	virtual void onNoInteract() = 0;
	virtual void onMouseTouch() = 0;

	const sf::RectangleShape& m_rectangle;

};

}


#endif //INTERFACE_BUTTON_HPP
