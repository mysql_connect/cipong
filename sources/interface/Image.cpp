#include "Image.hpp"

#include "renderer/RenderMaster.hpp"

namespace Interface {

Image::Image(const sf::Texture& texture, const sf::Vector2f size) {
	m_quad.setSize(size);
	m_quad.setTexture(&texture);
}

void Image::update(float deltaTime) {}

void Image::handleEvent(const sf::Event &event) {}

void Image::render(RenderMaster &renderer) noexcept {
	renderer.drawSFML(m_quad);
}

void Image::setPosition(const sf::Vector2f& position) {
	m_quad.setPosition(position);
}

const sf::Vector2f Image::getSize() const {
	return m_quad.getSize();
}

}