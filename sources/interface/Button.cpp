#include "Button.hpp"

namespace Interface {

Button::Button(const sf::RectangleShape& rectangle) : m_rectangle(rectangle) {}

void Button::update(float deltaTime) {
	if (touchingMouse(m_rectangle)) {
		onMouseTouch();
	} else {
		onNoInteract();
	}
}

void Button::handleEvent(const sf::Event& event) {
	if (clicked(m_rectangle, event)) {
		onClick();
	}
}

}

