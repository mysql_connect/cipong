#ifndef INTERFACE_MENU_HPP
#define INTERFACE_MENU_HPP

#include "Component.hpp"

namespace Interface {

class Menu {

public:
    Menu(const std::wstring& title);

    void addComponent(std::unique_ptr <Component> component);

    void update(float deltaTime);

    void handleEvent(const sf::Event &event);

    void render(RenderMaster &renderer);

    void clear();

private:
    float padding;
    sf::Text m_title;
    sf::RectangleShape m_background;
    std::vector <std::unique_ptr<Component>> m_components;

};

}


#endif //INTERFACE_MENU_HPP
