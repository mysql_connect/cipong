#include "Text.hpp"

#include "renderer/RenderMaster.hpp"

namespace Interface {

Text::Text(int size, const std::wstring& text) {
	initText(m_text , size, text);
}

void Text::update(float deltaTime) {}

void Text::handleEvent(const sf::Event& event) {}

void Text::render(RenderMaster& renderer) noexcept {
	renderer.drawSFML(m_text);
}

void Text::setPosition(const sf::Vector2f& position) {
	m_text.setPosition(position);
}

const sf::Vector2f Text::getSize() const {
	return {m_text.getLocalBounds().width, m_text.getLocalBounds().height};
}

}