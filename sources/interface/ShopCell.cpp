#include "ShopCell.hpp"

#include "Resource.hpp"
#include "renderer/RenderMaster.hpp"

namespace Interface {

ShopCell::ShopCell(const std::wstring& title, int price, const sf::Vector2f size) {
    m_quad.setSize(size);
    m_quad.setFillColor(sf::Color(0, 0, 0, 100));

    m_title.setString(title);
    m_title.setCharacterSize(15);
    m_title.setFont(g_resource->opensans);
    m_title.setFillColor(sf::Color::White);

    m_price.setCharacterSize(28);
    m_price.setFont(g_resource->opensans);
    m_price.setFillColor(sf::Color::White);
    m_price.setString(L"Цена: " + std::to_wstring(price) + L"$");

    m_status.setCharacterSize(14);
    m_status.setFont(g_resource->opensans);
    m_status.setFillColor(sf::Color::White);
    m_status.setString(L"Статус: ");
}

void ShopCell::addImage(const sf::Texture& texture, const sf::Vector2f size) {
    m_image.setTexture(&texture);
    m_image.setSize(size);
}

void ShopCell::setStatis(const std::wstring &status) {
    m_status.setString(L"Статус: " + status);
}


void ShopCell::update(float deltaTime) {

}

void ShopCell::handleEvent(const sf::Event &event) {

}

void ShopCell::render(RenderMaster& renderer) noexcept {
    renderer.drawSFML(m_quad);
    renderer.drawSFML(m_title);
    renderer.drawSFML(m_image);
    renderer.drawSFML(m_price);
    renderer.drawSFML(m_status);
}

void ShopCell::setPosition(const sf::Vector2f& position) {
    m_quad.setPosition(position); m_title.setPosition(position); m_image.setPosition(position); m_price.setPosition(position); m_status.setPosition(position);
    m_title.move(getSize().x / 2 - m_title.getGlobalBounds().width / 2, 5);
    m_image.move(getSize().x - m_image.getSize().x - 5, 35);
    m_price.move(15, getSize().y / 2 - m_price.getGlobalBounds().height / 2 - 30);
    m_status.move(15, getSize().y / 2);
}

const sf::Vector2f ShopCell::getSize() const {
    return m_quad.getSize();
}

}