#ifndef INTERFACE_INPUTBOX_HPP
#define INTERFACE_INPUTBOX_HPP

#include "Component.hpp"

namespace Interface {

class InputBox : public Component {

public:
    InputBox(unsigned maxTextLength, const sf::Vector2f size, const std::wstring& labelText, int& input);

    void update(float deltaTime) override ;
    void handleEvent(const sf::Event& event) override;
    void render(RenderMaster& renderer) override;

    void setPosition(const sf::Vector2f& position) override;

    const sf::Vector2f getSize() const;

private:
    sf::RectangleShape m_quad;
    sf::Text m_text;
    sf::Text m_label;

    int* m_inputted;

    unsigned m_maxLength;

    bool m_isActive = false;

};

}


#endif //INTERFACE_INPUTBOX_HPP
