#ifndef INTERFACE_IMAGE_HPP
#define INTERFACE_IMAGE_HPP

#include "Component.hpp"

namespace Interface {

class Image : public Component {

public:
	Image(const sf::Texture& texture, const sf::Vector2f size = {500, 200});

	void update(float deltaTime) override;
	void handleEvent(const sf::Event& event) override;
	void render(RenderMaster& renderer) noexcept override;

	void setPosition(const sf::Vector2f& position) override;

	const sf::Vector2f getSize() const override;

private:
	sf::RectangleShape m_quad;

};

}


#endif //INTERFACE_IMAGE_HPP
