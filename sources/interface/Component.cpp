#include "Component.hpp"

#include "Context.hpp"
#include "Resource.hpp"

namespace Interface {

bool Component::touchingMouse(const sf::Shape& shape) const {
	return shape.getGlobalBounds().contains(
			sf::Mouse::getPosition(*g_window).x,
			sf::Mouse::getPosition(*g_window).y
	);
}

bool Component::clicked(const sf::Shape& shape, const sf::Event& event) const {
	if (touchingMouse(shape)) {
		if(event.type == sf::Event::MouseButtonPressed) {
			return (event.mouseButton.button == sf::Mouse::Left);
		}
	}
	return false;
}

sf::Text Component::initText(int size, const std::wstring& title) {
	sf::Text text;
	initText(text, size, title);
	return text;
}

void Component::initText(sf::Text& text, int size, const std::wstring& title) {
	text.setString(title);
	text.setCharacterSize(size);
	text.setFont(g_resource->opensans);
}

}
