#ifndef INTERFACE_IMAGEBUTTON_HPP
#define INTERFACE_IMAGEBUTTON_HPP

#include <functional>

#include "Button.hpp"
#include "Image.hpp"

namespace Interface {

class ImageButton : public Button {

public:
	ImageButton(const sf::Texture& texture, const std::wstring& text, std::function<void(void)> function);

	void onClick() override;
	void onNoInteract() override;
	void onMouseTouch() override;

	void render(RenderMaster& renderer) noexcept override;

	void setPosition(const sf::Vector2f& position) override;

	const sf::Vector2f getSize() const override;

private:
	std::function<void(void)> m_function;
	sf::RectangleShape m_quad;
	Interface::Image m_image;
	sf::Text m_text;

};

}


#endif //INTERFACE_IMAGEBUTTON_HPP
