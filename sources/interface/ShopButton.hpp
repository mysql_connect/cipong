#ifndef INTERFACE_SHOPBUTTON_HPP
#define INTERFACE_SHOPBUTTON_HPP

#include <functional>

#include "Data.hpp"
#include "Button.hpp"

namespace Interface {

class ShopButton : public Button {

public:
	ShopButton(const std::wstring& text, std::function<void(int, Data&)> function, int id, Data& data);

	void onClick() override;
	void onNoInteract() override;
	void onMouseTouch() override;

	void render(RenderMaster& renderer) noexcept override;

	void setPosition(const sf::Vector2f& position) override;

	const sf::Vector2f getSize() const override;

private:
	std::function<void(int, Data&)> m_function;
	sf::RectangleShape m_quad;
	sf::Text m_text;

	Data* m_data;
	int m_id;
};

}


#endif //INTERFACE_SHOPBUTTON_HPP
