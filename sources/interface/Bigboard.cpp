#include "Bigboard.hpp"

#include "../Context.hpp"

namespace Interface {

Bigboard::Bigboard() {
	marginTop = (*g_window).getSize().y / 3;
}

void Bigboard::addComponent(std::unique_ptr<Component> component, int margin) {
	(*component).setPosition({(*g_window).getSize().x / 2 - (*component).getSize().x / 2, marginTop});
	marginTop += (*component).getSize().y + margin;
	m_bigboard.push_back(std::move(component));
}

void Bigboard::update(float deltaTime) {
	for (auto& component : m_bigboard) {
		(*component).update(deltaTime);
	}
}

void Bigboard::handleEvent(const sf::Event& event) {
	for (auto& component : m_bigboard) {
		(*component).handleEvent(event);
	}
}

void Bigboard::render(RenderMaster& renderer) {
	for (auto& component : m_bigboard) {
		(*component).render(renderer);
	}
}

void Bigboard::clear() {
	m_bigboard.clear();
	m_bigboard.shrink_to_fit();
}

}
