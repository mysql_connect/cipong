#ifndef INTERFACE_BASICBUTTON_HPP
#define INTERFACE_BASICBUTTON_HPP

#include <functional>

#include "Button.hpp"

namespace Interface {

class BasicButton : public Button {

public:
	BasicButton(const std::wstring& text, std::function<void(void)> function, int type = 0);

	void onClick() override;
	void onNoInteract() override;
	void onMouseTouch() override;

	void render(RenderMaster& renderer) noexcept override;

	void setPosition(const sf::Vector2f& position) override;

	const sf::Vector2f getSize() const override;

private:
	std::function<void(void)> m_function;
	sf::RectangleShape m_quad;
	sf::Text m_text;

	int m_type;
};

}


#endif //INTERFACE_BASICBUTTON_HPP
