#include "Menu.hpp"

#include "Context.hpp"
#include "Resource.hpp"
#include "renderer/RenderMaster.hpp"
#include "utils/ResourceHolder.hpp"

namespace Interface {

Menu::Menu(const std::wstring& title) {
    m_title.setString(title);
    m_title.setCharacterSize(20);
    m_title.setFont(g_resource->opensans);
    m_title.setFillColor(sf::Color::White);
    m_title.setPosition((*g_window).getSize().x / 2 - m_title.getGlobalBounds().width / 2, (*g_window).getSize().y / 2 - 115);

    m_background.setSize({240, 250});
    m_background.setOrigin(m_background.getSize() / 2.f);
    m_background.setFillColor(sf::Color(0, 0, 0, 128));
    m_background.setPosition((*g_window).getSize().x / 2, (*g_window).getSize().y / 2);

    padding = (*g_window).getSize().y / 2 - 40;
}

void Menu::addComponent(std::unique_ptr<Component> component) {
    (*component).setPosition({(*g_window).getSize().x / 2 - (*component).getSize().x / 2, padding});
    padding += 60;
    m_components.push_back(std::move(component));
}

void Menu::update(float deltaTime) {
    for (auto& component : m_components) {
        (*component).update(deltaTime);
    }
}

void Menu::handleEvent(const sf::Event& event) {
    for (auto& component : m_components) {
        (*component).handleEvent(event);
    }
}

void Menu::render(RenderMaster& renderer) {
    renderer.drawSFML(m_background);
    renderer.drawSFML(m_title);
    for (auto& component : m_components) {
        (*component).render(renderer);
    }
}

void Menu::clear() {
    m_components.clear();
    m_components.shrink_to_fit();
}

}
