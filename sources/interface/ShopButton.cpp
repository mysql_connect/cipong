#include "ShopButton.hpp"

#include "../renderer/RenderMaster.hpp"

namespace Interface {

ShopButton::ShopButton(const std::wstring& text, std::function<void(int, Data&)> function, int id, Data& data)
: Button(m_quad)
, m_function(function)
, m_data(&data) {
	m_id = id;
	initText(m_text, 16, text);
	m_quad.setSize({150, 40});

	m_quad.setFillColor(sf::Color(0, 0, 0, 128));
}

void ShopButton::onClick() {
	m_function(m_id, *m_data);
}

void ShopButton::onNoInteract() {
	m_quad.setFillColor(sf::Color(0, 0, 0, 128));
}

void ShopButton::onMouseTouch() {
	m_quad.setFillColor(sf::Color(255, 255, 255, 128));
}

void ShopButton::render(RenderMaster &renderer) noexcept {
	renderer.drawSFML(m_quad);
	renderer.drawSFML(m_text);
}

void ShopButton::setPosition(const sf::Vector2f &position) {
	m_quad.setPosition(position);
	m_text.setPosition(position);
	m_text.move(
			m_quad.getSize().x / 2 - m_text.getLocalBounds().width / 2,
			m_quad.getSize().y / 2 - m_text.getCharacterSize() / 2 - 2
	);
}

const sf::Vector2f ShopButton::getSize() const {
	return m_quad.getSize();
}

}
