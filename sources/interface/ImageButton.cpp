#include "ImageButton.hpp"

#include "renderer/RenderMaster.hpp"

namespace Interface {

ImageButton::ImageButton(const sf::Texture& texture, const std::wstring& text, std::function<void(void)> function)
:	Button(m_quad)
,	m_function(function)
,	m_image(texture, {25, 30}) {
	initText(m_text, 18, text);
	m_quad.setFillColor(sf::Color(255,255,255,0));
	m_quad.setSize({m_text.getLocalBounds().width + 35, 40});
}

void ImageButton::onClick() {
	m_function();
}

void ImageButton::onNoInteract() {}

void ImageButton::onMouseTouch() {}

void ImageButton::render(RenderMaster &renderer) noexcept {
	renderer.drawSFML(m_quad);
	renderer.drawSFML(m_text);
	m_image.render(renderer);
}

void ImageButton::setPosition(const sf::Vector2f &position) {
	m_quad.setPosition(position);

	m_text.setPosition(position);
	m_text.move(35, m_quad.getSize().y / 2 - m_text.getCharacterSize() / 2 - 2);

	m_image.setPosition({position.x, position.y + 5});
}

const sf::Vector2f ImageButton::getSize() const {
	return m_quad.getSize();
}

}
