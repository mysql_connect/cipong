#ifndef INTERFACE_TOGGLEBUTTON_HPP
#define INTERFACE_TOGGLEBUTTON_HPP

#include <utility>

#include "Button.hpp"
#include "renderer/RenderMaster.hpp"

namespace Interface {

template<typename T>
class ToggleButton : public Button {

public:
    ToggleButton(std::wstring&& title, const std::vector<std::pair<std::wstring, T>>& options, T& option)
    : Button(m_quad)
    , m_pOption(&option)
    , m_label(std::move(title)) {

        for (auto& option : options) {
            m_options.push_back(option.first);
            m_optionValues.push_back(option.second);
        }

        m_quad.setSize({230, 40});
        m_quad.setOutlineThickness(2);
        m_quad.setFillColor(sf::Color(100,100,100,100));
        m_quad.setOutlineColor(sf::Color::Black);

        T startOption = option;

        do {
            nextOption();
        } while (*m_pOption != startOption);
    }

    void onMouseTouch() {
        m_quad.setFillColor(sf::Color(255,255,255,100));
    }

    void onClick() {
        nextOption();
    }

    void onNoInteract() {
        m_quad.setFillColor(sf::Color(100,100,100,100));
    }

    void render(RenderMaster& renderer) override {
        renderer.drawSFML(m_quad);
        renderer.drawSFML(m_text);
    }

    void setPosition(const sf::Vector2f& position) override {
        m_quad.setPosition(position.x, position.y + 10);
        m_text.setPosition(m_quad.getPosition().x + 10, m_quad.getPosition().y + (m_quad.getSize().y / 2 - 12));
    }

    const sf::Vector2f getSize () const override {
        return m_quad.getSize();
    }

private:
    void nextOption() {
        m_optionIndex++;
        if (m_optionIndex > static_cast<int>(m_options.size() - 1)) {
            m_optionIndex = 0;
        }
        initText(m_text, 18, m_label + L": " + m_options[m_optionIndex]);
        *m_pOption = m_optionValues[m_optionIndex];
    }

    sf::RectangleShape m_quad;
    sf::Text m_text;

    std::vector<std::wstring> m_options;
    std::vector<T> m_optionValues;

    T* m_pOption;
    std::wstring m_label;
    int32_t m_optionIndex = -1;

};

}


#endif //INTERFACE_TOGGLEBUTTON_HPP
