#include <iostream>
#include "Background.hpp"

#include "Context.hpp"
#include "renderer/RenderMaster.hpp"

namespace Interface {

Background::Background(const Resource& resource) {
	std::string name;
	for (int i=1; i<=7; i++) {
		m_textures[i] = resource.backgrounds[i-1];
		scale = static_cast<float>((*g_window).getSize().y) / m_textures[i].getSize().y;

		m_sprites[i].setTexture(m_textures[i]);
		m_sprites[i].setScale(scale, scale);
		m_sprites[i].setOrigin(m_textures[i].getSize().x / 2, m_textures[i].getSize().y / 2);
		m_sprites[i].setPosition((*g_window).getSize().x / 2, (*g_window).getSize().y / 2);
	}
}

void Background::update(float deltaTime)  {
	m_sprites[2].move(-32 * deltaTime, 0);
	m_sprites[4].move( 48 * deltaTime, 0);
	m_sprites[6].move(-64 * deltaTime, 0);

	imgTempSize = {m_sprites[2].getTexture()->getSize().x * m_sprites[2].getScale().x, m_sprites[2].getTexture()->getSize().y * m_sprites[2].getScale().y};

	if (m_sprites[2].getPosition().x < (*g_window).getSize().x / 2 - (imgTempSize.x * 0.3542 - (imgTempSize.x / 2 - (*g_window).getSize().x / 2))) {
        m_sprites[2].setPosition((*g_window).getSize().x / 2 + (imgTempSize.x * 0.7881 - (imgTempSize.x / 2 - (*g_window).getSize().x / 2)), m_sprites[2].getPosition().y);
	}

    if (m_sprites[4].getPosition().x > (*g_window).getSize().x / 2 + (imgTempSize.x * 0.5554 - (imgTempSize.x / 2 - (*g_window).getSize().x / 2))) {
        m_sprites[4].setPosition((*g_window).getSize().x / 2 - (imgTempSize.x * 0.5560 - (imgTempSize.x / 2 - (*g_window).getSize().x / 2)), m_sprites[4].getPosition().y);
    }

    if (m_sprites[6].getPosition().x < (*g_window).getSize().x / 2 - (imgTempSize.x * 0.8346 - (imgTempSize.x / 2 - (*g_window).getSize().x / 2))) {
        m_sprites[6].setPosition((*g_window).getSize().x / 2 + (imgTempSize.x * 0.3227 - (imgTempSize.x / 2 - (*g_window).getSize().x / 2)), m_sprites[6].getPosition().y);
    }

}

void Background::render(RenderMaster &renderer) noexcept {
	for (int i=1; i<=7; ++i) {
		renderer.drawSFML(m_sprites[i]);
	}
}

}
