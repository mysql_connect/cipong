#ifndef INTERFACE_HEADER_HPP
#define INTERFACE_HEADER_HPP

#include "Component.hpp"

namespace Interface {

class Header {

public:
	Header();

	void addButton(std::unique_ptr<Component> component, float margin = 0, int type = 0);

	void update(float deltaTime);
	void handleEvent(const sf::Event& event);
	void render(RenderMaster& renderer);

	void clear();

private:
	float marginLeft, marginRight;

	std::vector<std::unique_ptr<Component>> m_header;

};

}

#endif //INTERFACE_HEADER_HPP
