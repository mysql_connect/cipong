#include "Header.hpp"

#include "Context.hpp"

namespace Interface {

Header::Header() {
	marginLeft = (*g_window).getSize().x / 2 - 500;
	marginRight = (*g_window).getSize().x / 2 + 500;
}

void Header::addButton(std::unique_ptr<Component> component, float margin, int type) {
	if (type == 0) {
		(*component).setPosition({marginLeft, 17});
		marginLeft += (*component).getSize().x + margin;
	} else {
		marginRight -= (*component).getSize().x + margin;
		(*component).setPosition({marginRight, 17});
	}

	m_header.push_back(std::move(component));
}

void Header::update(float deltaTime) {
	for (auto& component : m_header) {
		(*component).update(deltaTime);
	}
}

void Header::handleEvent(const sf::Event& event) {
	for (auto& component : m_header) {
		(*component).handleEvent(event);
	}
}

void Header::render(RenderMaster& renderer) {
	for (auto& component : m_header) {
		(*component).render(renderer);
	}
}

void Header::clear() {
	m_header.clear();
	m_header.shrink_to_fit();
}

}
