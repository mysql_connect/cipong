#include "Setting.hpp"

#include "Context.hpp"
#include "Resource.hpp"
#include "renderer/RenderMaster.hpp"

namespace Interface {

Setting::Setting(const std::wstring& title, sf::Vector2f size) {
    m_background.setSize(size);
    m_background.setFillColor(sf::Color(0,0,0,128));
    m_background.setPosition(
            (*g_window).getSize().x / 2 - size.x / 2,
            (*g_window).getSize().y / 2 - size.y / 2 + 40
    );

    m_title.setString(title);
    m_title.setCharacterSize(20);
    m_title.setFont(g_resource->opensans);
    m_title.setFillColor(sf::Color::White);
    m_title.setPosition(
            m_background.getPosition().x + m_background.getSize().x / 2 - m_title.getGlobalBounds().width / 2,
            m_background.getPosition().y + 20
    );
}

void Setting::addComponent(std::unique_ptr<Component> component, sf::Vector2f position) {
    (*component).setPosition({m_background.getPosition().x + position.x, m_background.getPosition().y + position.y});
    m_components.push_back(std::move(component));
}

void Setting::update(float deltaTime) {
    for (auto& component : m_components) {
        (*component).update(deltaTime);
    }
}

void Setting::handleEvent(const sf::Event &event) {
    for (auto& component : m_components) {
        (*component).handleEvent(event);
    }
}

void Setting::render(RenderMaster& renderer) {
    renderer.drawSFML(m_background);
    renderer.drawSFML(m_title);
    for (auto& component : m_components) {
        (*component).render(renderer);
    }
}

void Setting::clear() {
    m_components.clear();
    m_components.shrink_to_fit();
}

sf::Vector2f Setting::getSize() {
    return {m_background.getSize().x, m_background.getSize().y};
}

}