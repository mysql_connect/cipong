#include "BasicButton.hpp"

#include "../renderer/RenderMaster.hpp"

namespace Interface {

BasicButton::BasicButton(const std::wstring& text, std::function<void(void)> function, int type)
:	Button(m_quad)
,	m_function(function)
,	m_type(type) {

	if (type == 4) {
		initText(m_text, 16, text);
		m_quad.setSize({150, 40});
	} else if (type == 3) {
		initText(m_text, 18, text);
		m_quad.setSize({285, 50});
	} else if (type == 2) {
		initText(m_text, 26, text);
		m_quad.setSize({m_text.getLocalBounds().width + 60, 70});
	} else {
		initText(m_text, 14, text);
		m_quad.setSize({m_text.getLocalBounds().width + 40, 40});
	}

	if (m_type >= 3) {
		m_quad.setFillColor(sf::Color(0, 0, 0, 128));
	} else if (m_type >= 1) {
		m_quad.setFillColor(sf::Color(140, 193, 82, 255));
	} else {
		m_quad.setFillColor(sf::Color(255, 255, 255, 0));
	}
}

void BasicButton::onClick() {
	m_function();
}

void BasicButton::onNoInteract() {
	if (m_type >= 3) {
		m_quad.setFillColor(sf::Color(0, 0, 0, 128));
	} else if (m_type >= 1) {
		m_quad.setFillColor(sf::Color(140, 193, 82, 255));
	} else {
		m_quad.setFillColor(sf::Color(255, 255, 255, 0));
	}
}

void BasicButton::onMouseTouch() {
	if (m_type >= 3) {
		m_quad.setFillColor(sf::Color(255, 255, 255, 128));
	} else if (m_type >= 1) {
		m_quad.setFillColor(sf::Color(160, 212, 104, 255));
	} else {
		m_quad.setFillColor(sf::Color(255, 255, 255, 64));
	}
}

void BasicButton::render(RenderMaster &renderer) noexcept {
	renderer.drawSFML(m_quad);
	renderer.drawSFML(m_text);
}

void BasicButton::setPosition(const sf::Vector2f &position) {
	m_quad.setPosition(position);
	m_text.setPosition(position);
	m_text.move(
			m_quad.getSize().x / 2 - m_text.getLocalBounds().width / 2,
			m_quad.getSize().y / 2 - m_text.getCharacterSize() / 2 - 2
	);
}

const sf::Vector2f BasicButton::getSize() const {
	return m_quad.getSize();
}

}
