#ifndef INTERFACE_COMPONENT_HPP
#define INTERFACE_COMPONENT_HPP

#include <SFML/Graphics.hpp>

class RenderMaster;

namespace Interface {

class Component {

public:
	Component() {};
	virtual ~Component() {};

	virtual void update(float deltaTime) = 0;
	virtual void handleEvent(const sf::Event& event) = 0;
	virtual void render(RenderMaster& renderer) = 0;

	virtual void setPosition(const sf::Vector2f& position) = 0;

	virtual const sf::Vector2f getSize() const = 0;

protected:
	bool touchingMouse(const sf::Shape& shape) const;
	bool clicked(const sf::Shape& shape, const sf::Event& event) const;

	sf::Text initText(int size, const std::wstring& title);
	void initText(sf::Text& text, int size, const std::wstring& title);

};

class Empty : public Component {

public:
	void update(float deltaTime) {};
	void handleEvent(const sf::Event& event) {};
	virtual void render(RenderMaster& renderer) noexcept {};

	void setPosition(const sf::Vector2f& position) {};

	const sf::Vector2f getSize() const {return {0, 0};};

};

};


#endif //INTERFACE_COMPONENT_HPP
