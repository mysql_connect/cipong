#ifndef INTERFACE_BACKGROUND_HPP
#define INTERFACE_BACKGROUND_HPP

#include <vector>
#include <SFML/Graphics.hpp>

#include "Resource.hpp"

class RenderMaster;

namespace Interface {

class Background {

public:
	Background(const Resource& resource);

	void render(RenderMaster& renderer) noexcept;
	void update(float deltaTime);

private:
	float scale;

	sf::Sprite m_sprites[8];
	sf::Texture m_textures[8];

    sf::Vector2f imgTempSize;

};

}


#endif //INTERFACE_BACKGROUND_HPP
