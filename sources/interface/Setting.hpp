#ifndef INTERFACE_SETTING_HPP
#define INTERFACE_SETTING_HPP

#include "Component.hpp"

namespace Interface {

class Setting {

public:
    Setting(const std::wstring& titile, sf::Vector2f size);

    void addComponent(std::unique_ptr<Component> component, sf::Vector2f position);

    void update(float deltaTime);

    void handleEvent(const sf::Event &event);

    void render(RenderMaster &renderer);

    sf::Vector2f getSize();

    void clear();

private:
    sf::Text m_title;
    sf::RectangleShape m_background;
    std::vector<std::unique_ptr<Component>> m_components;

};

}

#endif //INTERFACE_SETTING_HPP
