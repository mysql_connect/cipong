#include "InputBox.hpp"

#include <iostream>

#include "renderer/RenderMaster.hpp"

namespace Interface {

InputBox::InputBox(unsigned maxTextLength, const sf::Vector2f size, const std::wstring& labelText, int& input)
: m_inputted(&input)
, m_maxLength(maxTextLength) {
    m_quad.setSize(size);
    m_quad.setFillColor(sf::Color(0, 0, 0, 100));

    m_quad.setOutlineThickness(2);
    m_quad.setOutlineColor(sf::Color::Black);

    initText(m_text, 18, std::to_wstring(input));
    initText(m_label, 14, labelText);
}

void InputBox::update(float deltaTime) {
    if (*m_inputted != 0) {
        m_text.setString(std::to_wstring(*m_inputted));
    } else {
        m_text.setString(L" ");
    }
}

void InputBox::handleEvent(const sf::Event& event) {
    if (event.type == sf::Event::TextEntered) {
        if (m_isActive) {
            wchar_t code = event.text.unicode;
            if (code == 8 && std::to_wstring(*m_inputted).length() >= 1) {
                if (*m_inputted < 10) {
                    *m_inputted = 0;
                } else {
                    *m_inputted = *m_inputted / 10;
                }
            } else if (code >= 48 && code <= 57 && std::to_wstring(*m_inputted).length() < m_maxLength) {
                if (*m_inputted != 0 ) {
                    *m_inputted = *m_inputted * 10 + (code - 48);
                } else {
                    *m_inputted = code - 48;
                }
            }
        }
    }
    if (event.type == sf::Event::MouseButtonPressed) {
        if (event.mouseButton.button == sf::Mouse::Left) {
            if (touchingMouse(m_quad)) {
                m_quad.setFillColor(sf::Color(255, 255, 255, 100));
                m_isActive = true;
            } else {
                m_quad.setFillColor(sf::Color(0, 0, 0, 100));
                m_isActive = false;
            }
        }
    }
}

void InputBox::render(RenderMaster& renderer) {
    renderer.drawSFML(m_quad);
    renderer.drawSFML(m_text);
    renderer.drawSFML(m_label);
}

void InputBox::setPosition(const sf::Vector2f& position) {
    m_label.setPosition(position);
    m_quad.setPosition(position.x, position.y + 20);
    m_text.setPosition(m_quad.getPosition().x + 10, m_quad.getPosition().y + (m_quad.getSize().y / 2 - 12));
}

const sf::Vector2f InputBox::getSize() const {
    return {m_quad.getSize().x, m_quad.getSize().y + 15};
}

}

