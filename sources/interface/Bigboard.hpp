#ifndef INTERFACE_BIGBOARD_HPP
#define INTERFACE_BIGBOARD_HPP

#include "Component.hpp"

namespace Interface {

class Bigboard {

public:
	Bigboard();

	void addComponent(std::unique_ptr<Component> component, int margin = 0);

	void update(float deltaTime);
	void handleEvent(const sf::Event& event);
	void render(RenderMaster& renderer);

	void clear();

private:
	float marginTop;
	std::vector<std::unique_ptr<Component>> m_bigboard;

};

}

#endif //INTERFACE_BIGBOARD_HPP
