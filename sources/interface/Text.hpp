#ifndef INTERFACE_TEXT_HPP
#define INTERFACE_TEXT_HPP

#include "Component.hpp"

namespace Interface {

class Text : public Component {

public:
	Text(int size, const std::wstring& text);

	void update(float deltaTime) override;
	void handleEvent(const sf::Event& event) override;
	void render(RenderMaster& renderer) noexcept override;

	void setPosition(const sf::Vector2f& position) override;

	const sf::Vector2f getSize() const override;

private:
	sf::Text m_text;

};

}


#endif //INTERFACE_TEXT_HPP
