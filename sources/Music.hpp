#ifndef CIPONG_MUSIC_HPP
#define CIPONG_MUSIC_HPP

#include <SFML/Audio.hpp>

#include "Resource.hpp"

class Music {

public:
    Music(const Resource& resource);

    void update();

private:
    sf::Sound m_sound;

    bool active = false;

};


#endif //CIPONG_MUSIC_HPP
