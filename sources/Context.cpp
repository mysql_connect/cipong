#include "Context.hpp"

#include <GL/glew.h>

#include "Config.hpp"

sf::RenderWindow* g_window;

Context::Context(const Settings& settings) {
	// Создаем переменную с параметрами контеrста OpenGL(depthBits, stenciBits, antialiasingLevel, majorVersion. minorVersion)
	sf::ContextSettings contextSettings(24, 8, settings.antialiasing, 3, 3);

	std::string title = static_cast<std::string>(PROJECT_NAME) + " - " + static_cast<std::string>(PROJECT_VERSION) + " [" + static_cast<std::string>(MINGW_VERSION) + "/" + static_cast<std::string>(CMAKE_BUILD_TYPE) +"]";

	// Создаем окно
	if (settings.isFullscreen) {
		window.create(sf::VideoMode::getDesktopMode(), title, sf::Style::Fullscreen, contextSettings);
	} else {
		if (settings.windowX >= 1000 && settings.windowY >= 700) {
			window.create(sf::VideoMode(settings.windowX, settings.windowY), title, sf::Style::Close, contextSettings);
		} else if(settings.windowX == 0 && settings.windowX == 0) {
			window.create(sf::VideoMode(1000, 700), title, sf::Style::None, contextSettings);
		} else {
			window.create(sf::VideoMode(1000, 700), title, sf::Style::Close, contextSettings);
		}
	}

	// Вкл/Выкл вертикальную синхронизацию
	if (settings.verticalSync) {
		window.setVerticalSyncEnabled(true);
	} else {
		window.setVerticalSyncEnabled(false);
	}

	// Указываем ограничение по фпс
	window.setFramerateLimit(120);

	g_window = &window;

	// Иницилизируем GLEW
/*	glewExperimental = GL_TRUE;
	GLenum glew = glewInit();
	if (glew != GLEW_OK) {
		throw std::runtime_error("GLEW init failed.");
	}*/

	// Создаем и отрисовываем содержимое окна viewport(x, y, width, height)
	glViewport(0, 0, window.getSize().x, window.getSize().y);

	// Отключаем отрисовку задней части приметивов
	glCullFace(GL_BACK);

	// Включаем смешивание цветов (Альфа-канал/Прозрачность)
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
