#include <fstream>
#include <iostream>

#include "Data.hpp"
#include "Settings.hpp"
#include "Application.hpp"

/* Включаем определение видеокарт */
extern "C" {
	__declspec(dllexport) bool NvOptimusEnablement = true;
	__declspec(dllexport) bool AmdPowerXpressRequestHighPerformance = true;
}

// Загрузка данных
void loadData(Data& data) {

    std::ifstream inDataFile("data.bin", std::ios::binary);
    if (!inDataFile.is_open()) {
        std::ofstream outDataFile("data.bin", std::ios::binary);
        data.money = 200;data.rockets[0] = true; data.balls[0] = true; data.ball = 0; data.rocket = 0;
        for (int i = 1; i < 5; ++i) {
            data.rockets[i] = false; data.balls[i] = false;
        }
        outDataFile.write((char*)&data.money, sizeof(data.money));
        outDataFile.write((char*)data.balls, sizeof(data.balls));
        outDataFile.write((char*)data.rockets, sizeof(data.rockets));
        outDataFile.write((char*)&data.ball, sizeof(data.ball));
        outDataFile.write((char*)&data.rocket, sizeof(data.rocket));
        outDataFile.close();
    }

    inDataFile.read((char*)&data.money, sizeof(data.money));
    inDataFile.read((char*)data.balls, sizeof(data.balls));
    inDataFile.read((char*)data.rockets, sizeof(data.rockets));
    inDataFile.read((char*)&data.ball, sizeof(data.ball));
    inDataFile.read((char*)&data.rocket, sizeof(data.rocket));
    inDataFile.close();

}

// Загрузка настроек
void loadSettings(Settings& settings) {
	std::string key;
	std::ifstream configFile("settings.txt");

	if (configFile.is_open()) {
		while (configFile >> key) {
			if (key == "fullscreen") {
				configFile >> settings.isFullscreen;
				std::cout << "Config: Full screen mode - " << std::boolalpha << settings.isFullscreen << std::endl;
			} else if (key == "issoundplay") {
				configFile >> settings.isSoundPlay;
				std::cout << "Config: Sound play - " << std::boolalpha << settings.isSoundPlay << std::endl;
			} else if (key == "verticalsync") {
				configFile >> settings.verticalSync;
				std::cout << "Config: Vertical Syns mode - " << std::boolalpha << settings.verticalSync << std::endl;
			} else if (key == "antiliasing") {
				configFile >> settings.antialiasing;
				std::cout << "Config: Antiliasing level - "  << settings.antialiasing << std::endl;
			} else if (key == "windowsize") {
				configFile >> settings.windowX >> settings.windowY;
				std::cout << "Config: Windows size - "  << settings.windowX << "x" << settings.windowY << std::endl;
			}
		}
	} else {
		std::cout << "Config: Could not find config.txt file! Using defaults." << std::endl;
	}
}

/* Точка входа */
int main(int argc, char* argv[]) {
	try {
		Settings settings;
		loadSettings(settings);

		Data data;
		loadData(data);

		Application application(settings, data);
		application.runGameLoop();
	} catch (std::exception& error) {
		std::cout << "Error: " << error.what() << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}