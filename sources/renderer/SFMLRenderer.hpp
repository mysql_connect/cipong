#ifndef SFMLRENDERER_HPP
#define SFMLRENDERER_HPP

#include <vector>
#include <SFML/Graphics.hpp>

class SFMLRenderer {

public:
	SFMLRenderer();

	void add(const sf::Drawable& drawable);
	void render(sf::RenderWindow& window);

private:
	std::vector<const sf::Drawable*> m_drawables;

};


#endif //SFMLRENDERER_HPP
