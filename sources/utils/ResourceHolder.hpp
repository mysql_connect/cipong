#ifndef RESOURCEHOLDER_HPP
#define RESOURCEHOLDER_HPP

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include "Singleton.hpp"
#include "ResourceManager.hpp"

class ResourceHolder : public Singleton {

public:
	static ResourceHolder& get();

	static const sf::Font& getFont(const std::string& fileName);
	static const sf::Texture& getTexture(const std::string& fileName);
	static const sf::SoundBuffer& getSoundBuffer(const std::string& fileName);

	ResourceManager<sf::Font> fonts;
	ResourceManager<sf::Texture> textures;
	ResourceManager<sf::SoundBuffer> soundBuffers;

private:
	ResourceHolder() noexcept;

};


#endif //RESOURCEHOLDER_HPP
