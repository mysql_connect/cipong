#ifndef RESOURCEMANAGER_HPP
#define RESOURCEMANAGER_HPP

#include <string>
#include <unordered_map>

template<typename Resource>
class ResourceManager {

public:
	ResourceManager(const std::string& folder, const std::string& ext) : m_folder("resource/" + folder + "/"), m_ext("." + ext) {}

	const Resource& get(const std::string& fileName) {
		if (!exits(fileName)) {
			addResource(fileName);
		}
		return m_resourceMap[fileName];
	}

	void remove(const std::string& fileName) {
		m_resourceMap.erase(fileName);
	}

private:
	void addResource(const std::string& fileName) {
		Resource resource;
		resource.loadFromFile(buildString(fileName));
		m_resourceMap.emplace(fileName, resource);
	}

	std::string buildString(const std::string& fileName) {
		return m_folder + fileName + m_ext;
	}

	bool exits(const std::string& fileName) const {
		return  m_resourceMap.find(fileName) != m_resourceMap.end();
	}

	std::unordered_map<std::string, Resource> m_resourceMap;
	std::string m_folder, m_ext;

};


#endif //RESOURCEMANAGER_HPP
