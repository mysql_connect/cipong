#include "Random.hpp"

RandomSingleton& RandomSingleton::get() {
    static RandomSingleton random;
    return random;
}

RandomSingleton::RandomSingleton() {
    m_randomEngine.seed(std::time(nullptr));
    for (int i = 0; i < 5; i++) {
        intInRage(i, i * 5);
    }
}
