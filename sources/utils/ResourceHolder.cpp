#include "ResourceHolder.hpp"

#include <iostream>

ResourceHolder::ResourceHolder() noexcept
:	fonts("fonts", "ttf")
,	textures("textures", "png")
,	soundBuffers("sounds", "ogg") {

}

ResourceHolder &ResourceHolder::get() {
	static ResourceHolder resource;
	return resource;
}

const sf::Font& ResourceHolder::getFont(const std::string& fileName) {
	std::cout << "Load Font: " << fileName.c_str() << std::endl;
	return get().fonts.get(fileName);
}

const sf::Texture& ResourceHolder::getTexture(const std::string& fileName) {
	std::cout << "Load Texture: " << fileName.c_str() << std::endl;
	return get().textures.get(fileName);
}

const sf::SoundBuffer& ResourceHolder::getSoundBuffer(const std::string& fileName) {
	std::cout << "Load SoundBuffer: " << fileName.c_str() << std::endl;
	return get().soundBuffers.get(fileName);
}
