#ifndef SETTINGS_HPP
#define SETTINGS_HPP


struct Settings {

	int windowX 		= 1280;
	int windowY 		= 720;
	int antialiasing 	= 8;
	bool isFullscreen 	= false;
	bool isSoundPlay 	= false;
	bool verticalSync 	= true;

};

extern Settings g_settings;

#endif //SETTINGS_HPP