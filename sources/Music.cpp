#include "Music.hpp"

#include "Settings.hpp"

Music::Music(const Resource& resource) {
    m_sound.setVolume(4);
    m_sound.setLoop(true);
    m_sound.setBuffer(resource.backsound);
}

void Music::update() {

    if (!active && g_settings.isSoundPlay) {
        active = true;
        m_sound.play();
    }

    if (active && !g_settings.isSoundPlay) {
        active = false;
        m_sound.stop();
    }

}
